# Hash Ping-pocket

Litle utility app based on electron js to hash passwords and automatiquely ping user's list of url.

Necessary app relative json files can be found in `[home directory]/hashed-ping-pocket/`.

## Quick start

```bash
yarn install
yarn start
```
### Build the app

```bash
yarn make
```

## Dev, style gestion

### Lint

```bash
yarn style:lint
yarn style:fix
```

### Sass command

```bash
sass --watch assets/styles/scss/index.scss:assets/styles/style.css
```

