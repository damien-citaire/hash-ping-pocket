const { app, BrowserWindow } = require("electron");

require("@electron/remote/main").initialize();

const path = require("path");

let winArray = [];

function createHomeWindow() {
    winArray["index"] = new BrowserWindow({
        width: 1200,
        height: 800,
        icon: path.join(__dirname, 'assets/images/icon.png'),
        webPreferences: {
            nodeIntegration: true,
            contextIsolation: false,
            enableRemoteModule: true,
        },
    });

    winArray["index"].loadFile(path.join(__dirname, "index.html"));
}

app.whenReady().then(createHomeWindow);

app.on("window-all-closed", () => {
    if (process.platform !== "darwin") {
        app.quit();
    }
});

app.on("activate", () => {
    if (BrowserWindow.getAllWindows().length === 0) {
        createHomeWindow();
    }
});
