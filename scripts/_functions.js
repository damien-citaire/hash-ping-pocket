function showNotification(title, message, silent = true) {
  new Notification({
    title: title,
    body: message,
    icon: path.join(__dirname, 'assets/images/icon.png'),
    silent: silent,
  }).show();
}
