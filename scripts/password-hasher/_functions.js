function encryptor(password) {
  for (let i = 0, l = $algos.length; i < l; i++) {
    const $algo = $algos[i];
    const $encrypt = crypto.createHash($algo.algorithm).update(password, 'binary').digest('hex');
    $algo.inputResult.innerText = $encrypt;
  }
}

function emptyInputs() {
  for (let i = 0, l = $algos.length; i < l; i++) {
    const $algo = $algos[i];
    if ($algo.inputResult.innerText !== 0) {
      $algo.inputResult.innerText = '';
    }
  }
}

function copyPassword() {
  for (let i = 0, l = $algos.length; i < l; i++) {
    const $algo = $algos[i];
    const $hashedPassword = $algo.inputResult.innerText;
    const $chosenAlgo = $algo.algorithm;

    $algo.copyButton.addEventListener('click', () => {
      clipboard.writeText($hashedPassword, 'selection');
      showNotification(
        'Copié !',
        `Le mot de passe hashé en ${$chosenAlgo} à bien été copié`,
      );
    });
  }
}
