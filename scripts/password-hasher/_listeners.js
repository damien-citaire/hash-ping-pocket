$wordToHashForm.addEventListener('submit', (e) => {
  e.preventDefault();
  if ($wordToHashInput.value.length == 0) {
    emptyInputs();
    $hashedWord.innerText = '';
    $resultSection.style.display = 'none';
    $wordToHashInput.classList.add('is-invalid');
    $wordToHashFormFeedbakError.innerText = "Vous n'envoyez aucun mot se faire hasher menu";
  } else {
    emptyInputs();
    $hashedWord.innerText = $wordToHashInput.value;
    $wordToHashInput.classList.remove('is-invalid');
    $wordToHashFormFeedbakError.innerText = '';
    $resultSection.style.display = 'block';
    encryptor($wordToHashInput.value);
    copyPassword();
  }
});

$closeHashResults.addEventListener('click', () => {
  emptyInputs();
  $resultSection.style.display = 'none';
  $wordToHashInput.value = '';
  $hashedWord.innerText = '';
});
