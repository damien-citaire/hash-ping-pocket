// Search section
const $wordToHashForm = document.querySelector('#wordToHashForm');
const $wordToHashInput = document.querySelector('#wordToHashInput');
const $wordToHashFormFeedbakError = document.querySelector('#wordToHashFormFeedbakError');
// Results section
const $resultSection = document.querySelector('#result-section');
const $closeHashResults = $resultSection.querySelector('#closeHashResults');
const $hashedWord = $resultSection.querySelector('#hashedWord');
const $inputHashedMd5 = $resultSection.querySelector('#md5-output');
const $inputHashedSha1 = $resultSection.querySelector('#sha1-output');
const $inputHashedSha256 = $resultSection.querySelector('#sha256-output');
const $inputHashedSha512 = $resultSection.querySelector('#sha512-output');
// CopyButtons
const $cpBtnHashedMd5 = $resultSection.querySelector('#md5-cpBtn');
const $cpBtnHashedSha1 = $resultSection.querySelector('#sha1-cpBtn');
const $cpBtnHashedSha256 = $resultSection.querySelector('#sha256-cpBtn');
const $cpBtnHashedSha512 = $resultSection.querySelector('#sha512-cpBtn');

const $algos = [
  {
    algorithm: 'md5',
    inputResult: $inputHashedMd5,
    copyButton: $cpBtnHashedMd5,
  },
  {
    algorithm: 'sha1',
    inputResult: $inputHashedSha1,
    copyButton: $cpBtnHashedSha1,
  },
  {
    algorithm: 'sha256',
    inputResult: $inputHashedSha256,
    copyButton: $cpBtnHashedSha256,
  },
  {
    algorithm: 'sha512',
    inputResult: $inputHashedSha512,
    copyButton: $cpBtnHashedSha512,
  },
];

$resultSection.style.display = 'none';
