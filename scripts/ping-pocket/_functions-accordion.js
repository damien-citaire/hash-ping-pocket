function generateAccordionItem(title, url) {
  const $index = generateIndex(title);
  const $accordionItem = `
        <div class="accordion-item" id="accordion-item-${$index}">
            <h3 class="accordion-header">
                <button
                    class="accordion-button collapsed"
                    type="button"
                    data-bs-toggle="collapse"
                    data-bs-target="#collapse-${$index}"                                                        
                >
                    <span class="text-primary1">
                        ${title}
                    </span>
                    <small class="ms-2 text-muted">${url}</small>
                </button>
                <div class="accordion-actions">
                    <button
                        class="roundedButton roundedButton_warning"
                        id="update-${$index}"
                        type="button"   
                        onclick="updateAccordionItem('${title}', '${url}');"
                        title="Rafraichir cette adresse"                                                   
                    >
                        <i class="material-icons">
                            refresh
                        </i>
                    </button>
                    <button
                        class="roundedButton roundedButton_danger"
                        id="delete-${$index}"
                        type="button"  
                        onclick="deleteAccordionItem('${title}', '${$index}');"  
                        title="Supprimer cette adresse"                                                  
                    >
                        <i class="material-icons">
                            delete_outline
                        </i>
                    </button>
                </div>
            </h3>
            <div
                id="collapse-${$index}"
                class="accordion-collapse collapse"                        
                data-bs-parent="#dashboardAccordion"
            >
                <div class="accordion-body">
                </div>
            </div>
        </div>
    `;
  $dashboardAccordion.innerHTML += $accordionItem;
}

function generateResponseLine(title, response, containerResult) {
  const $sectionResponse = `
        <div class="row result">
            <div class="col-sm-3">
                <strong>${title}</strong>
            </div>
            <div class="col-sm-9">
                ${response}
            </div>
        </div>
    `;
  containerResult.innerHTML += $sectionResponse;
}

function generatePingResponse(title, url, container = null) {
  let $item = null;
  let $body = null;
  let $loading = null;
  let $aestheticTimeout = 0;
  const $classPending = 'results_pending';
  const $classSuccess = 'results_success';
  const $classError = 'results_error';
  const $classesResult = [$classSuccess, $classError];

  if (container == null) {
    $aestheticTimeout = 200;

    // get accordionItem parts to manage style & append content
    const $index = generateIndex(title);
    $item = $dashboardAccordion.querySelector(`#accordion-item-${$index}`);
    $body = $item.querySelector('.accordion-body');

    // style loading gestion
    $item.classList.add($classPending);
    const $classesList = $item.classList;
    // Reset Success or error style
    $classesResult.forEach((cssClass) => {
      if ($classesList.contains(cssClass)) {
        $item.classList.remove(cssClass);
      }
    });
  } else {
    $aestheticTimeout = 1000;
    $item = container;
    $body = container;
    $loading = document.querySelector('#spinner');
    $loading.style.display = 'block';
  }

  setTimeout(() => {
    ping.promise.probe(url).then((res) => {
      if (res.alive == true) {
        generateResponseLine('Url', res.host, $body);
        generateResponseLine('Adresse IP', res.numeric_host, $body);
        generateResponseLine('En ligne', res.alive, $body);
        generateResponseLine('Ping', res.time, $body);

        $item.classList.remove($classPending);
        $item.classList.add($classSuccess);
        $loading.style.display = 'none';
      } else {
        showNotification('Attention !', `l'adresse ${res.host} ne réponds pas !`, false);
        generateResponseLine('Url', res.host, $body);
        generateResponseLine('Adresse IP', res.numeric_host, $body);
        generateResponseLine('En ligne', res.alive, $body);

        $item.classList.remove($classPending);
        $item.classList.add($classError);
        $loading.style.display = 'none';
      }
    });
  }, $aestheticTimeout);
}

function generateFullAccordionItem(title, url) {
  const $generateAccordionItem = new Promise((resolve, reject) => {
    resolve(generateAccordionItem(title, url));
  });
  $generateAccordionItem.then(() => {
    // generate and append each accordion item content
    generatePingResponse(title, url);
  });
}

function generateAccordion() {
  for (let $i = 0, $l = $watchtower.length; $i < $l; $i++) {
    const $target = $watchtower[$i];
    generateFullAccordionItem($target.title, $target.url);
  }
}

function deleteAccordionItem(title, index) {
  const $accordionTarget = $dashboardSection.querySelector(`#accordion-item-${index}`);
  const $arrayEntryTarget = $watchtower.findIndex((entry) => entry.title === title);

  $watchtower.splice($arrayEntryTarget, 1);
  $accordionTarget.parentElement.removeChild($accordionTarget);
  writeJsonFile('ping-url-list.json', $watchtower);
  refreshSomeItemsDisplaying();
}

function updateAccordionItem(title, url) {
  const $index = generateIndex(title);
  const $itemBody = $dashboardAccordion.querySelector(`#accordion-item-${$index} .accordion-body`);

  $itemBody.innerHTML = '';
  generatePingResponse(title, url);
}
