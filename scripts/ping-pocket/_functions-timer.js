function getTimeRemaining(endtime) {
  const total = Date.parse(endtime) - Date.parse(new Date());
  const seconds = Math.floor((total / 1000) % 60);
  const minutes = Math.floor((total / 1000 / 60) % 60);
  const hours = Math.floor((total / (1000 * 60 * 60)) % 24);

  return {
    total,
    hours,
    minutes,
    seconds,
  };
}

function initializeClock(endtime) {
  function updateClock() {
    $timeRemaining = getTimeRemaining(endtime);

    $hoursSpan.innerHTML = (`0${$timeRemaining.hours}`).slice(-2);
    $minutesSpan.innerHTML = (`0${$timeRemaining.minutes}`).slice(-2);
    $secondsSpan.innerHTML = (`0${$timeRemaining.seconds}`).slice(-2);

    if ($timeRemaining.total <= 0) {
      clearInterval($timeinterval);
    }
  }

  $timeinterval = setInterval(updateClock, 1000);
  updateClock();
}

function stopTimer() {
  $timeRemaining.seconds = 0;
  $timeRemaining.minutes = 0;
  $timeRemaining.hours = 0;
  $timeRemaining.total = 0;
  clearInterval($timeinterval);
  $hoursSpan.innerHTML = '00';
  $minutesSpan.innerHTML = '00';
  $secondsSpan.innerHTML = '00';
}

// --------

function initTimer() {
  // get time values & convert to seconds + put in $pingInterval
  const $hoursInSeconds = $pingIntervalHours * 3600;
  const $minutesInSeconds = $pingIntervalMinutes * 60;
  const $totalIntervalInSeconds = $hoursInSeconds + $minutesInSeconds;

  const $deadline = new Date(Date.parse(new Date(Date.now() + $totalIntervalInSeconds * 1000)));
  initializeClock($deadline);
}

function launchTime() {
  const $task = new Task('Regenerate accordion', () => {
    clearInterval($timeinterval);
    initTimer();
    $dashboardAccordion.innerHTML = '';
    generateAccordion();
  });

  const $job = new SimpleIntervalJob(
    { hours: $pingIntervalHours, minutes: $pingIntervalMinutes, seconds: 0 },
    $task,
  );
  scheduler.addSimpleIntervalJob($job);
}

function generateChoiceTimer(min, max, selector) {
  for (let $i = min, $l = max + 1; $i < $l; $i++) {
    const $option = `<option value="${$i}">${$i}</option>`;
    selector.innerHTML += $option;
  }
}

function updateTimeInterval() {
  $timeInterval.hours.replace($pingIntervalHours);
  $timeInterval.minutes.replace($pingIntervalMinutes);
  const $newTimeInterval = { hours: $pingIntervalHours, minutes: $pingIntervalMinutes };
  writeJsonFile('ping-time-interval.json', $newTimeInterval);
}

function stopCountdown() {
  $stopTimer.style.display = 'none';
  $playTimer.style.display = 'flex';
  $countdownClock.style.display = 'none';
  $countdownClockMessage.style.display = 'block';

  scheduler.stop();
  stopTimer();
}

function restartCountdown() {
  $playTimer.style.display = 'none';
  $stopTimer.style.display = 'flex';
  $countdownClock.style.display = 'flex';
  $countdownClockMessage.style.display = 'none';

  initTimer(); // launch timer for 1st session
  launchTime(); // launch scheduler wich re-launch timer for all next sessions
}
