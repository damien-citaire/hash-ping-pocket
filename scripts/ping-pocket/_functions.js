function validateName(name) {
  const $regex = /[^a-zA-Z0-9 ]/g;
  return ($isValid = $regex.test(name));
}

function generateIndex(title) {
  return ($index = title.replace(/[^a-zA-Z0-9]/g, ''));
}

function createUserFiles() {
  // Generate list of url file
  if (!fs.existsSync(path.join(pathDir, 'ping-url-list.json'))) {
    fs.writeFile(path.join(pathDir, 'ping-url-list.json'), '[]', (err) => {
      if (err) {
        return console.log(err);
      }
      console.log('The list file was saved!');
    });
  }
  // Generate time interval file
  if (!fs.existsSync(path.join(pathDir, 'ping-time-interval.json'))) {
    fs.writeFile(
      path.join(pathDir, 'ping-time-interval.json'),
      '{"hours": "0", "minutes": "1"}',
      (err) => {
        if (err) {
          return console.log(err);
        }
        console.log('The time interval file was saved!');
      },
    );
  }
}

function appFileInit() {
  if (!fs.existsSync(pathDir)) {
    fs.mkdir(pathDir, (err) => {
      if (err) {
        return console.error(err);
      }
      console.log('Directory created successfully!');
    });
    createUserFiles();
  }

  if (fs.existsSync(pathDir)) {
    createUserFiles();
  }

  // Timeout ensures that we have our created files at time
  setTimeout(() => {
    $watchtower = JSON.parse(fs.readFileSync(`${pathDir}/ping-url-list.json`, 'utf8'));

    $timeInterval = JSON.parse(fs.readFileSync(`${pathDir}/ping-time-interval.json`, 'utf8'));
    $pingIntervalHours = $timeInterval.hours;
    $pingIntervalMinutes = $timeInterval.minutes;
  }, 300);
}

function refreshSomeItemsDisplaying() {
  $watchtower.length <= 1
    ? ($sortResults.style.display = 'none')
    : ($sortResults.style.display = 'flex');

  if ($watchtower.length == 0) {
    $noUrlMessage.style.display = 'block';
    $refreshResults.style.display = 'none';
    $editTimerPopoverActivator.style.display = 'none';
    stopCountdown();
    $countdown.style.opacity = 0;
    $playTimer.disabled = true;
  } else {
    $noUrlMessage.style.display = 'none';
    $refreshResults.style.display = 'block';
    $editTimerPopoverActivator.style.display = 'flex';
    $countdown.style.opacity = 1;
    $playTimer.disabled = false;
  }
}

function writeJsonFile(targetFile, data) {
  fs.writeFile(path.join(pathDir, targetFile), JSON.stringify(data), (err) => {
    if (err) {
      return console.log(err);
    }
  });
}

function addEntry(title, url) {
  const $newEntry = { title, url };
  $watchtower.push($newEntry);
  writeJsonFile('ping-url-list.json', $watchtower);
}

function resetUrlToPingResult() {
  $urlToPingResults.innerHTML = '';
  $urlToPingResults.className = 'urlToPingResults';
  $urlToPingResults.style.display = 'none';
  $urlToPingActions.style.display = 'none';
}
