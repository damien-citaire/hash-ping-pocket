$urlToPingForm.addEventListener('submit', (e) => {
  e.preventDefault();
  if ($urlToPingInput.value.length == 0) {
    resetUrlToPingResult();
    $urlToPingInput.classList.add('is-invalid');
    $urlToPingFormFeedbakError.innerText = "Vous n'envoyez aucune url à pinger";
  } else {
    // url validation
    const $regex = /[-a-zA-Z0-9._-]{1,256}\.[a-zA-Z()]{1,10}/g;
    const $checkUrl = $regex.test($urlToPingInput.value);

    if ($checkUrl == false) {
      resetUrlToPingResult();
      $urlToPingFormFeedbakError.innerText = "Format de l'url invalide";
      $urlToPingInput.classList.add('is-invalid');
    } else {
      // update input value to validated url
      const $validatedUrl = $urlToPingInput.value.match($regex);
      $urlToPingInput.value = $validatedUrl[0];

      resetUrlToPingResult();
      $urlToPingInput.classList.remove('is-invalid');
      $urlToPingFormFeedbakError.innerText = '';
      generatePingResponse(null, $urlToPingInput.value, $urlToPingResults);
      $urlToPingResults.style.display = 'block';
      $urlToPingActions.style.display = 'flex';

      $urlToPingActionClose.addEventListener('click', () => {
        $urlToPingInput.value = null;
        resetUrlToPingResult();
        $popoverAddUrl.hide();
      });
    }
  }
});

$addUrlPopoverActivator.addEventListener('shown.bs.popover', () => {
  const $urlToPingInputName = document.querySelector('#urlToPingInputName');
  const $urlToPingAddForm = document.querySelector('#urlToPingAddForm');
  const $urlToPingAddFormFeedbakError = document.querySelector('#urlToPingAddFormFeedbakError');
  const $urlToAdd = $urlToPingInput.value;

  $urlToPingInput.disabled = true;
  $urlToPingSubmit.disabled = true;

  $urlToPingAddForm.addEventListener('submit', (e) => {
    e.preventDefault();
    const $nameAllreadyExist = $watchtower.some((el) => el.title === $urlToPingInputName.value);

    if (
      validateName($urlToPingInputName.value) == true
            || $urlToPingInputName.value.length == 0
    ) {
      $urlToPingInputName.classList.add('is-invalid');
      $urlToPingAddFormFeedbakError.innerText = 'Votre nom est vide ou contient des caractères spéciaux et/ou accentués, choisissez-en un autre';
    } else if ($nameAllreadyExist == true) {
      $urlToPingInputName.classList.add('is-invalid');
      $urlToPingAddFormFeedbakError.innerText = 'Vous avez déjà une entrée avec ce nom, choisissez-en un autre';
    } else {
      $urlToPingInputName.classList.remove('is-invalid');
      $urlToPingAddFormFeedbakError.innerText = '';
      $urlToPingInput.value = null;
      resetUrlToPingResult();
      addEntry($urlToPingInputName.value, $urlToAdd);
      generateFullAccordionItem($urlToPingInputName.value, $urlToAdd);
      $popoverAddUrl.hide();
      $urlToPingInput.disabled = false;
      $urlToPingSubmit.disabled = false;
      refreshSomeItemsDisplaying();
    }
  });
});

$addUrlPopoverActivator.addEventListener('hidden.bs.popover', () => {
  $urlToPingInput.disabled = false;
  $urlToPingSubmit.disabled = false;
});

$stopTimer.addEventListener('click', () => {
  stopCountdown();
});

$playTimer.addEventListener('click', () => {
  restartCountdown();
});

$editTimerPopoverActivator.addEventListener('shown.bs.popover', () => {
  const $selectHours = document.querySelector('#selectHours');
  const $selectMinutes = document.querySelector('#selectMinutes');
  const $submitInterval = document.querySelector('#submitInterval');

  generateChoiceTimer(0, 23, $selectHours);
  generateChoiceTimer(1, 59, $selectMinutes);

  $submitInterval.addEventListener('click', () => {
    // first stop current timer and task
    scheduler.stop();
    stopTimer();

    // refresh interval indicator time
    $definedIntervalHours.innerHTML = (`0${$selectHours.value}`).slice(-2);
    $definedIntervalMinutes.innerHTML = (`0${$selectMinutes.value}`).slice(-2);

    // update critical variables
    $pingIntervalHours = $selectHours.value;
    $pingIntervalMinutes = $selectMinutes.value;

    // write updated critical variables to json file
    updateTimeInterval();

    // relaunch updated timer and task
    restartCountdown();

    // hide popover
    $popoverEditTimer.hide();
  });
});

$refreshResults.addEventListener('click', () => {
  $dashboardAccordion.innerHTML = '';
  generateAccordion();
});

$sortResults.addEventListener('click', () => {
  const $direction = $dashboardAccordion.style.flexDirection;
  const $icon = $sortResults.querySelector('.material-icons');

  if ($direction == 'column-reverse') {
    $dashboardAccordion.style.flexDirection = 'column';
    $icon.innerText = 'south';
  } else if ($direction == 'column') {
    $dashboardAccordion.style.flexDirection = 'column-reverse';
    $icon.innerText = 'north';
  }
});
