//
// Selectors
// ---------
// UrlToPing selectors
const $urlToPingSection = document.querySelector('#urlToPingSection');
const $urlToPingInput = $urlToPingSection.querySelector('#urlToPingInput');
const $urlToPingForm = $urlToPingSection.querySelector('#urlToPingForm');
const $urlToPingSubmit = $urlToPingSection.querySelector('#urlToPingSubmit');
const $urlToPingFormFeedbakError = $urlToPingSection.querySelector('#urlToPingFormFeedbakError');
const $urlToPingActions = $urlToPingSection.querySelector('#urlToPingActions');
const $urlToPingActionClose = $urlToPingActions.querySelector('#urlToPingActionClose');
const $urlToPingResults = $urlToPingSection.querySelector('#urlToPingResults');
const $addUrlPopoverActivator = $urlToPingActions.querySelector('#addUrlPopoverActivator');
// Dashboard selectors
const $dashboardSection = document.querySelector('#dashboardSection');
const $dashboardAccordion = $dashboardSection.querySelector('#dashboardAccordion');
const $noUrlMessage = $dashboardSection.querySelector('#noUrlMessage');
const $refreshResults = $dashboardSection.querySelector('#refreshResults');
const $sortResults = $dashboardSection.querySelector('#sortResults');
const $stopTimer = $dashboardSection.querySelector('#stopTimer');
const $playTimer = $dashboardSection.querySelector('#playTimer');
const $editTimerPopoverActivator = $dashboardSection.querySelector('#editTimerPopoverActivator');
const $countdownClock = $dashboardSection.querySelector('#countdownClock');
const $countdownClockMessage = $dashboardSection.querySelector('#countdownClockMessage');
const $definedIntervalHours = $dashboardSection.querySelector('#definedIntervalHours');
const $definedIntervalMinutes = $dashboardSection.querySelector('#definedIntervalMinutes');
// Dashboard clock timer selectors
const $countdown = $dashboardSection.querySelector('#countdown');
const $hoursSpan = $countdown.querySelector('#clockHours');
const $minutesSpan = $countdown.querySelector('#clockMinutes');
const $secondsSpan = $countdown.querySelector('#clockSeconds');

//
// Critical variables
// ------------------
let $watchtower = null;
let $timeInterval = null;
let $pingIntervalHours = 0;
let $pingIntervalMinutes = 1;

//
// Popovers html contents
// -----------------------
const $addUrlTitle = `
    Ajoutez un nom à votre nouvelle URL
    <button class='roundedButton roundedButton_danger popover-closeBtn' onclick=$popoverAddUrl.hide()>
        <i class="material-icons material-icons_sm">close</i>
    </button>
`;

const $addUrlForm = `
    <form id="urlToPingAddForm">
        <div class="input-group">
            <input
                id="urlToPingInputName"
                class="form-control"
                type="text"
                name="urlToPingInput"
                placeholder="nom"
            />
            <button id="urlToPingActionAdd" class="btn btn-success">
                <i class="material-icons material-icons_sm text-white">check</i>
            </button>
            <div id="urlToPingAddFormFeedbakError" class="invalid-feedback"></div>
        </div>
    </form>
`;

const $editTimerTitle = `
    Definissez un nouvel interval entre les pings
    <button class='roundedButton roundedButton_danger popover-closeBtn' onclick=$popoverEditTimer.hide()>
        <i class="material-icons material-icons_sm">close</i>
    </button>
`;

const $editTimerForm = `
    <form>
        <div class="input-group input-group-sm">
            <select class="form-select" id="selectHours">
            </select>
            <span class="input-group-text">h</span>
            <select class="form-select" id="selectMinutes">
            </select>
            <span class="input-group-text">m</span>
            <button class="btn btn-success" type="button" id="submitInterval">
                <i class="material-icons material-icons_sm text-white">check</i>
            </button>
        </div>
    </form>
`;

//
// Init Popovers
// -------------
const $popoverAddUrl = new bootstrap.Popover($addUrlPopoverActivator, {
  container: 'body',
  content: $addUrlForm,
  title: $addUrlTitle,
  placement: 'auto',
  trigger: 'click',
  html: true,
  sanitize: false,
});

const $popoverEditTimer = new bootstrap.Popover($editTimerPopoverActivator, {
  container: 'body',
  content: $editTimerForm,
  title: $editTimerTitle,
  placement: 'bottom',
  trigger: 'click',
  html: true,
  sanitize: false,
});

//
// Init Tooltips
// -------------
const tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'));
const tooltipList = tooltipTriggerList.map((tooltipTriggerEl) => new bootstrap.Tooltip(tooltipTriggerEl));
