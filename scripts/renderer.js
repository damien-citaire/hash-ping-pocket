const { Notification } = require('@electron/remote');
const fs = require('fs');
const os = require('os');
const path = require('path');

const pathDir = path.join(os.homedir(), 'hashed-ping-pocket');
const { clipboard } = require('electron');
const crypto = require('crypto');
const { ToadScheduler, SimpleIntervalJob, Task } = require('toad-scheduler');

const scheduler = new ToadScheduler();
const ping = require('ping');

appFileInit();

setTimeout(() => {
  // Launch timer for 1st session
  initTimer();
  // Launch scheduler wich re-launch timer for all next sessions
  launchTime();
  refreshSomeItemsDisplaying();
  generateAccordion();

  $definedIntervalHours.innerHTML = (`0${$pingIntervalHours}`).slice(-2);
  $definedIntervalMinutes.innerHTML = (`0${$pingIntervalMinutes}`).slice(-2);
}, 300);
